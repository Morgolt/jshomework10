const insetsWindow = document.querySelector('.tab');
const forAll = document.querySelectorAll('.tab-content li');
insetsWindow.addEventListener('click', (event) => {
  console.log(insetsWindow.querySelector('.active'));
  insetsWindow.querySelector('.active').classList.remove('active');
  event.target.classList.add('active');
  const elemId = event.target.id;
  forAll.forEach(item => {
    item.hidden = true;
    if (elemId === item.dataset.filter) {
      item.hidden = false;
    }
  });
});
